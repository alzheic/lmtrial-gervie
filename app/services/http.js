/**
 * HTTP Service: HTTP
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .service('HttpService', HttpService);

  function HttpService($http) {
    var vm = this;

    vm.SetAuthorizationHeader = SetAuthorizationHeader;
    vm.RemoveAuthHeaders = RemoveAuthHeaders;

    function SetAuthorizationHeader(token) {
      $http.defaults.headers.common.Authorization = 'Bearer ' + token;
    }

    function RemoveAuthHeaders() {
      $http.defaults.headers.common.Authorization = '';
    }
  }
})();