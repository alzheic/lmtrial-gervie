/**
 * Contact Service: Contact
 */
 (function() {
  'use strict';

  angular.module('lmtrial')
         .service('ContactService', ContactService);

  function ContactService($http, $q, $rootScope, UserSession, API_PATH, CONTACT) {
    var service = {
      RetrieveListFamilyData: RetrieveListFamilyData
    };

    return service;

    function RetrieveListFamilyData() {
      var d = $q.defer();

      $http({
        method: 'GET',
        url: API_PATH.URL + CONTACT.GET_ALL_FAMILY + '?startWith=*',
        headers: {
          'Authorization': 'Bearer ' + $rootScope.token
        }
      })
      .success(function(data) {
        d.resolve(data);
      })
      .error(function(response) {
        if(response == "Your Session has expired") {
          UserSession.Logout();
        }
        d.reject(response);
      });

      return d.promise;
    }
  }
 })();