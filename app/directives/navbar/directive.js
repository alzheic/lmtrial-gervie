/**
 * Angular Directive: navbar
 */
(function() {
  'use strict';

  angular.module('lmtrial')
         .directive('navbar', navbar);

  function navbar($rootScope, NG_PATH, UserSession) {
    var directive = {
      restrict: 'E',
      templateUrl: NG_PATH.DIRECTIVES + 'navbar/template.html',
      link: _Link
    };

    return directive;

    function _Link(scope, element, attrs) {
      scope.ToggleMenu = ToggleMenu;
      scope.Logout = Logout;
      $rootScope.IsToggleMenuClosed = false;

      function ToggleMenu() {
        $rootScope.IsToggleMenuClosed = !$rootScope.IsToggleMenuClosed;
      }

      function Logout() {
        UserSession.Logout();
      }
    }
  }
})();