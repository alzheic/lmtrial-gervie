/**
 * Angular Directive: sidebar
 */
 (function() {
  'use strict';

  angular.module('lmtrial')
         .directive('sidebar', sidebar);

  function sidebar($rootScope, $state, NG_PATH) {
    var directive = {
      restrict: 'E',
      templateUrl: NG_PATH.DIRECTIVES + 'sidebar/template.html',
      link: _Link
    }

    return directive;

    function _Link($scope, element, attrs) {
      // console.log($state);
    }
  }
 })();