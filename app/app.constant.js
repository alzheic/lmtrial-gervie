/*
 * Angular Dependencies: lmtrial.constants
 */
(function(){
  'use strict';
  angular.module('lmtrial')
    .constant('APP_PATH', {
      BASE_URL: '/contacts',
      LOGIN_URL: '/login'
    })
    .constant('NG_PATH', {
      CONTROLLERS: 'app/controllers/',
      DIRECTIVES: 'app/directives/',
      FACTORIES: 'app/factories/',
      SERVICES: 'app/services/'
    })
    .constant('API_PATH', {
      URL: 'https://testapi.nzfsg.co.nz/'
    })
    .constant('AUTH', {
      LOGIN: 'login'
    })
    .constant('CONTACT', {
      GET_ALL_FAMILY: 'contacts/FamilyListGet'
    })
    .constant('COOKIE', {
      AUTH_TOKEN: 'auth_token'
    });
})();